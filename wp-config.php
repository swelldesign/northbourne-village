<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nbv001' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lOH$TC4GF5s}BY;/Po#DT=O6QqFbsXf$Y`!pHT#H@L;[Wk9VHB#/*qoL1vSlk)_j' );
define( 'SECURE_AUTH_KEY',  '<=h.K*8$ )nBx>y2X%~Y{$1(p(j#>vA=1G *vNN;DIiA;[?0`(|z.J4yTA;VJj,R' );
define( 'LOGGED_IN_KEY',    'sYcB;^yMm?~mchmC_;eUNL44gL`/D*P)vDCL5:Yve|&rnBlJ|V*Oz*$w,*kg!8,0' );
define( 'NONCE_KEY',        '4Dxzy1c{t{^|+zzMX:/n,ae|M1d)|Hdb91}6uK^ YQ>5IU9UkduyhGI?%QZJ |A%' );
define( 'AUTH_SALT',        'Q]Hpj~|{dB0/`l7voP^NW4uCEQAH3`sr8ARFi}>9M=)E.bEZIn/x6<-gfQrV=~*<' );
define( 'SECURE_AUTH_SALT', 'C9m$bJ6{v+`&EWL%wD/@U`_GiA=Sj_oCVu!m9B{?*7ST.lOSkqBbsKB,=InUutV!' );
define( 'LOGGED_IN_SALT',   '9~`YHZyjw649ed+iCm<$v;.8M!do?KYaR5< Cgnu`KGK{i#&dn&LDnjv2rFJxTT$' );
define( 'NONCE_SALT',       'T;If*@m.<`#H9)5-[q+&.3#50rCn!b;w!rdO!.E_:KrISUk U?{w+2q;Q8w@,UB)' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
