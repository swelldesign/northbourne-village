<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if (has_post_thumbnail()) : ?>
			<div class="entry-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div>
		<?php endif; ?>

		<?php if (is_single()) : ?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
			<h1 class="entry-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h1>
		<?php endif; ?>

		<div class="entry-meta">
			<?php
			printf('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author vcard"><a class="url fn" href="%3$s" title="%4$s" rel="author">%5$s</a></span> <span class="amp">&</span> filed under %6$s.',
				get_the_time('Y-m-j'),
				get_the_time(get_option('date_format')),
				esc_url(get_author_posts_url(get_the_author_meta('ID'))),
				esc_attr(sprintf('View all posts by %s', get_the_author())),
				get_the_author(),
				get_the_category_list(', '));
			?>
		</div>
	</header>

	<?php if (is_search()) : ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div>
	<?php else : ?>
		<div class="entry-content">
			<?php
			the_content('Continue reading <span class="meta-nav">&rarr;</span>');
			wp_link_pages();
			?>
		</div>
	<?php endif; ?>

	<footer class="entry-meta">
		<?php /* Comments info. */ ?>
		<?php if (comments_open() && !is_single()) : ?>
			<div class="comments-link">
				<?php comments_popup_link('<span class="leave-reply">' . 'Leave a comment' . '</span>', 'One comment so far', 'View all % comments'); ?>
			</div>
		<?php endif; ?>
	</footer>
</article>