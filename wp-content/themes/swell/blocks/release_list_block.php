<?php
$id = $data['id'];
$introduction = $data['introduction'];

?>
<section <?= !empty($id) ? 'id="' . strtolower($id) . '"' : '' ?> class="release_list_block b-relative bg-white padding-120 ani ani-slide-up">
    <div class="outter">
        <div class="introduction">
            <?= $introduction ?>
        </div>
        <div class="projects ani ani-slide-up">
            <?php $num = get_option('posts_per_page'); ?>
            <?php $projects = get_posts_by_cates(0, $num);

            foreach ($projects as $key => $p) :
            ?>

                <div class="project-box <?= $p->release ?>">
                    <a href="<?= $p->url ?>">
                        <div class="project-inner">
                            <?php if (!empty($p->image)) : ?>
                                <div class="flex-box">
                                    <div class="l-box">
                                        <div class="header"><?= $p->header ?></div>
                                        <div class="logo"><img src="<?= $p->logo ?>" /></div>
                                        <div class="content"><?= $p->content ?></div>
                                        <div class="link font-nv-bold">LEARN MORE</div>
                                    </div>
                                    <div class="r-box bg-load" data-bg-load="<?= $p->image ?>">
                                    </div>
                                    <div class="logo-image">
                                        <img src="<?php __a('/resources/artwork/icon-' . $p->release . '-hover.svg') ?>" width="180" />
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="no-image-box">
                                    <h5 class="header">COMING SOON</h5>
                                    <h3>STAGE THREE</h3>
                                    <div class="content"><?= $p->content ?></div>
                                    <div class="link">VIEW ALL</div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>

            <?php endforeach; ?>
        </div>
    </div>
</section>