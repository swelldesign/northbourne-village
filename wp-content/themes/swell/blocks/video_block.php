<?php
$id = $data['id'];
$image = $data['image'];
$vimeo_video_id = $data['vimeo_video_id'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="video_block border-l-r ani ani-slide-up" data-video='<div class="videobg vimeo">
    <iframe class="vimeo" src="https://player.vimeo.com/video/<?= $vimeo_video_id; ?>?&loop=1&byline=0&title=1&sidedock=0&autoplay=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'>
    <div class="post-image">
        <?php $original_image_url = str_replace('-scaled', '', $image['url']); ?>
        <div class="bg-load <?= $key == 0 ? 'active' : ''; ?>" data-bg-load="<?= $original_image_url ?>"></div>
        <div class="btn"></div>
    </div>
</section>