<?php
$id = $data['id'];
$svg_ani = $data['svg_ani'];
$svg = 'ani/' . $svg_ani['color'] . '/' . $svg_ani['svg'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="svg_animation_block b-relative border padding-60 bg-white ani ani-slide-up">
    <div class="outter">
        <div class="svg">
            <div class="svg-ani" data-path="<?php __a($svg) ?>">
                <div id="lottie"></div>
            </div>
        </div>
    </div>
</section>