<?php
$id = $data['id'];
$position = $data['image_content_position'];
$content = $data['content'];
$image = $data['image'];
$background_color = $data['background_color'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="image_content_block b-relative border-l-r ani ani-slide-up <?= $position ?>">
    <div class="flex-box-eq-2 no-gap">
        <div class="l-box <?= $background_color ?>">
            <div class="box">
                <div class="header"><?= $content['header'] ?></div>
                <div class="title">
                    <h3><?= $content['title'] ?></h3>
                </div>
                <div class="text"><?= $content['text'] ?></div>
            </div>
        </div>
        <div class="r-box">
            <img class="lazy-loading" data-src="<?= $image['url'] ?>" alt="image" />
        </div>
    </div>
</section>