<?php
$id = $data['id'];
$title = $data['title'];
$items = $data['content'];

//__l($items);
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="content_slider_block_b content_slider_block border-l-r bg-grey ani ani-slide-up">

    <div class="swiper-carousel swiper-container">
        <div class="swiper-wrapper">
            <?php foreach ($items as $i => $p) : ?>
                <div class="swiper-slide" data-subtitle="<?= $p['header'] ?>" data-swiper-slide-index="<?= $i  ?>">
                    <div class="flex-box-eq-2 no-gap item-box">
                        <div class="l-box">
                            <div class="item">
                                <?php $original_image_url = str_replace('-scaled', '', $p['image']['url']); ?>
                                <img class="lazy-loading" data-src="<?= $original_image_url ?>" width="100%" alt="image" />
                            </div>
                        </div>
                        <div class="r-box">
                            <div class="item">
                                <div class="item-header">
                                    <h6><?= $title ?></h6>
                                </div>
                                <div class="item-content">
                                    <div class="content">
                                        <p><?= $p['text'] ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="nav">
            <div class="flex-box-eq-2">
                <div class="l-box">

                </div>
                <div class="r-box">
                    <div class="carousel-nav">
                        <div class="container">
                            <ul class="swiper-pagination font-nv-bold"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</section>