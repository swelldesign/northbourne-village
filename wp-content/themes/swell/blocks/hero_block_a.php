<?php
$content = $data['content'];
$svg_ani = $data['svg_ani'];
$svg = 'ani/' . $svg_ani['color'] . '/' . $svg_ani['svg'];
?>
<section class="hero_block_a b-relative border bg-grey vh-100 ani ani-slide-up">
    <div class="outter">
        <div class="svg">
            <div class="svg-ani" data-path="<?php __a($svg) ?>">
                <div id="lottie"></div>
            </div>
        </div>
        <div class="flex-box-2">
            <div class="l-box"></div>
            <div class="r-box">
                <img src="<?php __a('resources/artwork/home-title.svg') ?>" />
            </div>
        </div>
    </div>
</section>