<?php
$id = $data['id'];
$header = $data['header'];
$floorplans = $data['floorplan'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="floorplan_block content_slider_block b-relative bg-white padding-120 ani ani-slide-up">
    <div class="outter">
        <div class="swiper-carousel swiper-container bg-lt-grey">
            <div class="swiper-wrapper">
                <?php foreach ($floorplans as $i => $p) : ?>
                    <div class="swiper-slide" data-subtitle="<?= $p['title'] ?>" data-swiper-slide-index="<?= $i  ?>">
                        <div class="flex-box-eq-2 item-box">

                            <div class="l-box">
                                <div class="item">
                                    <div class="item-content">
                                        <div class="header font-ssp"><?= $header ?></div>
                                        <div class="nav">
                                            <div class="carousel-nav">
                                                <div class="container">
                                                    <a class="swl-select" href="#">
                                                        <ul class="swiper-pagination font-nv-bold"></ul>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details">
                                            <div class="bed">
                                                <span class="font-ssp">Bedrooms</span>
                                                <?= $p['bed']; ?>
                                            </div>
                                            <div class="bath">
                                                <span class="font-ssp">Bathrooms</span>
                                                <?= $p['bath']; ?>
                                            </div>
                                            <div class="car_spaces">
                                                <span class="font-ssp">Car Space</span>
                                                <?= $p['car_spaces']; ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item-content">

                                        <div class="content">
                                            <div class="text">
                                                <p><?= $p['text']; ?></p>
                                            </div>
                                        </div>
                                        <div class="link">
                                            <a class="font-nv-bold" href="<?= $p['pdf']['url'] ?>">Download Floorplan PDF</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="r-box">
                                <div class="item">
                                    <?php $original_image_url = str_replace('-scaled', '', $p['floorplan']['url']); ?>
                                    <img class="lazy-loading" data-src="<?= $original_image_url ?>" alt="image" />
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</section>