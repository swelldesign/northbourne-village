<?php
$id = $data['id'];
$content = $data['content'];
$image = $data['image'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="complex_content_block b-relative bg-white padding-120 ani ani-slide-up">
    <div class="outter">
        <div class="content">
            <?= $content ?>
        </div>
        <div class="image-box">
            <div class="post-image">
                <?php $original_image_url = str_replace('-scaled', '', $image['url']); ?>
                <div class="bg-load <?= $key == 0 ? 'active' : ''; ?>" data-bg-load="<?= $original_image_url ?>"></div>
            </div>
            <div class="image-caption">
                <div class="caption">
                    <?= !empty($image['caption']) ? $image['caption'] : 'Artist Impression'; ?>
                </div>
            </div>
        </div>
    </div>
</section>