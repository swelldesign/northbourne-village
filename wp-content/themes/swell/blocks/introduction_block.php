<?php
$id = $data['id'];
$header = $data['header'];
$content = $data['content'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="introduction_block b-relative border-l-r ani ani-slide-up padding-120">
    <div class="outter">
        <div class="flex-box-2">
            <div class="l-box">
                <?= $header ?>
            </div>
            <div class="r-box">
                <?= $content ?>
            </div>
        </div>
    </div>
</section>