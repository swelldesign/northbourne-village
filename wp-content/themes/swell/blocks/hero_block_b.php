<?php
global $page;
$content = $data['content'];
$bg_color = $data['background_color'];
$svg_ani = $data['svg_ani'];
$svg = 'ani/' . $svg_ani['color'] . '/' . $svg_ani['svg'];
?>
<section class="hero_block_b b-relative border vh-100 ani ani-slide-up <?= $bg_color ?>">
    <div class="lg-outter">
        <div class="content ani ani-slide-up">
            <h1><?= get_the_title($page->ID); ?></h1>
            <?= $content; ?>
        </div>
        <div class="svg">
            <div class="svg-ani" data-path="<?php __a($svg) ?>">
                <div id="lottie"></div>
            </div>
        </div>
    </div>
</section>