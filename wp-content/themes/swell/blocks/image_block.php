<?php
$id = $data['id'];
$image = $data['image'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="image_block b-relative border-l-r ani ani-slide-up">
    <div class="post-image">
        <?php $original_image_url = str_replace('-scaled', '', $image['url']); ?>
        <div class="bg-load <?= $key == 0 ? 'active' : ''; ?>" data-bg-load="<?= $original_image_url ?>"></div>
    </div>
</section>