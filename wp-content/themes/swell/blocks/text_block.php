<?php
$id = $data['id'];
$content = $data['content'];
?>
<section <?= !empty($id) ? 'id="' .  strtolower($id) . '"' : '' ?> class="text_block b-relative bg-white padding-120 ani ani-slide-up">
    <div class="outter">
        <div class="box">
            <?= $content ?>
        </div>
    </div>
</section>