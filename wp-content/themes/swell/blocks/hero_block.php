<?php
$background = $data['background'];
?>
<section id="home" class="hero_block b-relative ani ani-slide-up vh-100">
    <div class="post-image bg-box">
        <?php $original_image_url = str_replace('-scaled', '', $background['url']); ?>
        <div class="bg-load" data-bg-load="<?= $original_image_url ?>"></div>
        <div class="content wrapper-inner ani ani-fade-in">
            <img src="<?php __a('/resources/artwork/logo-w.svg') ?>" />
        </div>
    </div>
</section>