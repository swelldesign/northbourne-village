<?php
$header = $data['header'];
$images = $data['gallery'];
?>
<section class="gallery_block ani ani-slide-up">
    <div class="outter">
        <?php if (!empty($header)) : ?>
            <div class="header ani ani-slide-up">
                <h3><?= $header ?></h3>
            </div>
        <?php endif; ?>
        <div class="swiper-container swiper-slider ani ani-slide-up">
            <div class="swiper-wrapper">
                <?php foreach ($images as $key => $image) : ?>
                    <div class="swiper-slide">
                        <div class="post-image">
                            <?php $original_image_url = str_replace('-scaled', '', $image['url']); ?>
                            <div class="bg-load <?= $key == 0 ? 'active' : ''; ?>" data-bg-load="<?= $original_image_url ?>"></div>
                        </div>
                        <div class="image-title">
                            <div class="title font-nv-bold">
                                <?php echo $image['title']; ?>
                            </div>
                        </div>
                        <div class="image-caption">
                            <div class="caption">
                                <?= !empty($image['caption']) ? $image['caption'] : 'Artist Impression'; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- Add Arrows -->
            <!-- <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div> -->
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>

</section>