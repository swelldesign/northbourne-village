<?php
global $release;

$map_marker = !empty($release['name']) ? $release['name'] : 'nv';
?>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3YBWI5Y4aGuFRi6wExC2LME7NuoRUD4c"></script>


<section id="location" class="map_filter_block ani ani-slide-up" data-marker="<?= $map_marker ?>">
  <div class="outter">
    <?php
    /**
     * Template to render the location points of interest panel
     *
     * @author Swell Design Group <http://swelldesign.com.au/>
     * @package UnderSwell
     * @since UnderSwell 1.0
     */

    $locations = get_field('points_of_interest', 'options');

    if (!empty($locations)) {

      $filters = array(
        'DINING + CAFES' => 'restaurant,cafe',
        'CULTURAL' => 'entertainment,culture',
        'LIFESTYLE' => 'lifestyle',
        'SHOPPING' => 'shopping',
        'TRANSPORT' => 'transport',
        'SCHOOLS' => 'school',
      );

    ?>

      <div id="poi-map">
        <div id="poi-filter">
          <select id="poi-filter-select">
            <option value="">Show All</option>
            <?php
            foreach ($filters as $label => $filter) {
              echo '<option value="' . $filter . '">' . $label . '</option>';
            }
            ?>
          </select>
          <ul class="ul-filter">
            <?php
            foreach ($filters as $label => $filter) {
              echo '<li><a class="font-nv-bold" href="#" data-poi-filter="' . $filter . '">' . $label . '</a></li>';
            }
            ?>
          </ul>
        </div>
        <div id="pois">
          <?php
          foreach ($locations as $location) {
            echo '
						<div class="poi" data-lat="' . $location['location']['lat'] . '" data-lng="' . $location['location']['lng'] . '" data-cat="' . $location['category'] . '">
							<p class="poi-tooltip">' . $location['title'] . '</p>
						</div>';
          }
          ?>
        </div>
        <div id="poi-gmap"></div>
      </div>
  </div>

<?php

    }

?>
</section>