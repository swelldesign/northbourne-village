<?php
$items = $data['content'];

//__l($items);
?>
<section class="content_slider_block_a content_slider_block border-l-r bg-grey">
    <div class="outter">
        <div class="swiper-carousel swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($items as $i => $p) : ?>
                    <div class="swiper-slide" data-subtitle="<?= $p['header'] ?>" data-swiper-slide-index="<?= $i  ?>">
                        <div class="flex-box-eq-2 item-box">
                            <div class="l-box">
                                <div class="item">
                                    <div class="item-header font-nv"><?= $p['title'] ?></div>
                                    <div class="item-content">
                                        <div class="content">
                                            <p><?= $p['text'] ?></p>
                                        </div>
                                        <div class="link">
                                            <a class="font-nv-bold" href="<?= $p['link']['url'] ?>">Learn More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="r-box">
                                <div class="item">
                                    <?php

                                    $svg_ani = $p['svg_ani'];
                                    $svg = 'ani/' . $svg_ani['color'] . '/' . $svg_ani['svg'];
                                    ?>
                                    <div class="<?= $i == 0 ? 'svg-ani' : 'svg-ani-static'; ?>" data-path="<?php __a($svg) ?>">
                                        <div id="lottie"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="nav">
                <div class="flex-box-eq-2">
                    <div class="l-box">
                        <div class="carousel-nav">
                            <div class="container">
                                <ul class="swiper-pagination font-nv-bold">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>