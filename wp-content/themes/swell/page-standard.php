<?php

/**
 * Template Name: Standard Page
 */
?>
<?php get_header(); ?>

<?php
$p_id = $post->ID;
$blocks = get_field('blocks', $p_id);
$page = get_query_var('paged');
//__l($blocks);
SITE()->blocks($blocks);
?>
<?php get_footer(); ?>