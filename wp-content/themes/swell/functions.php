<?php


/**
 * THEME SETUP
 */

add_action('after_setup_theme', 'sk_theme_setup');
function sk_theme_setup()
{
    // Add theme supports.
    add_theme_support('menus');
    add_theme_support('post-thumbnails');
    add_theme_support('automatic-feed-links');
    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list'));
    sk_theme_supports();

    // Add default thumbnail size.
    /* set_post_thumbnail_size(48, 48, true); */

    // Actions.
    add_action('init', 'sk_head_cleanup');
    add_action('wp_enqueue_scripts', 'sk_scripts_styles');
    add_action('widgets_init', 'sk_register_sidebars');

    // Filters.
    add_filter('show_admin_bar', '__return_false');
    add_filter('wp_title', 'sk_wp_title', 10, 2);
}

function sk_theme_supports()
{
    register_nav_menus(array(
        'main-nav' => 'Main Nav',
        'footer-links' => 'Footer Links'
    ));
}

function sk_head_cleanup()
{
    // WP version, EditURI, Windows Live Writer link.
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');

    // CSS for recent comments widget.
    global $wp_widget_factory;
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}

function sk_scripts_styles()
{
    // Adds JS to pages with the comment form to support sites with threaded comments (if applicable).
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

function sk_register_sidebars()
{
    register_sidebar(array(
        'id' => 'sidebar-1',
        'name' => 'Sidebar One',
        'description' => 'Sidebar One',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));

    // To create another sidebar, copy-paste the sidebar code above and use it as a template.
    // To output a sidebar, simply use the following snippet:
    /* <?php dynamic_sidebar('sidebar-1'); ?> */
}

function sk_wp_title($title, $separator)
{
    // Format page title.
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo('name');

    // Add a page number if necessary.
    if ($paged >= 2 || $page >= 2) {
        $title = $title . ' ' . $separator . ' ' . sprintf('Page %s', max($paged, $page));
    }

    return $title;
}


/**
 * UTIL
 */

// Shorthand for echo.
function __p($val)
{
    echo $val;
}

// Shorthand for printing asset.
function __a($path)
{
    echo get_template_directory_uri() . '/' . $path;
}

// Shorthand for printing text content.
function __c($txt)
{
    echo apply_filters('the_content', $txt);
}

// Shorthand for printing log/dump.
function __l($val)
{
    echo '<pre>';
    print_r($val);
    echo '</pre>';
}

// Shorthand for including a file.
function __i($path)
{
    if (!empty($path)) {
        require_once get_template_directory() . '/' . $path;
    }
}


/**
 * OTHER CONFIG
 */

// Thumbnail sizes.
// add_image_size('sk-thumb-600', 600, 150, true);
// add_image_size('sk-thumb-300', 300, 100, true);

// Remove update notifications.
// add_filter('pre_site_transient_update_core', 'sk_remove_core_updates');
// add_filter('pre_site_transient_update_plugins', 'sk_remove_core_updates');
// add_filter('pre_site_transient_update_themes', 'sk_remove_core_updates');
// function sk_remove_core_updates() {
//     global $wp_version;
//     return (object) array(
//         'last_checked' => time(),
//         'version_checked' => $wp_version
//     );
// }

//disable WordPress from creating multiple thumbnails
// disable generated image sizes
function shapeSpace_disable_image_sizes($sizes)
{

    unset($sizes['thumbnail']);    // disable thumbnail size
    unset($sizes['medium']);       // disable medium size
    unset($sizes['large']);        // disable large size
    unset($sizes['medium_large']); // disable medium-large size
    unset($sizes['1536x1536']);    // disable 2x medium-large size
    unset($sizes['2048x2048']);    // disable 2x large size
    return $sizes;
}
add_action('intermediate_image_sizes_advanced', 'shapeSpace_disable_image_sizes');

// disable scaled image size
add_filter('big_image_size_threshold', '__return_false');

// disable other image sizes
function shapeSpace_disable_other_image_sizes()
{
    remove_image_size('post-thumbnail'); // disable images added via set_post_thumbnail_size() 
    remove_image_size('another-size');   // disable any other added image sizes

}
add_action('init', 'shapeSpace_disable_other_image_sizes');

// disable new wordpress Gutenberg editor
add_filter('use_block_editor_for_post_type', function ($enabled, $post_type) {
    return ('post' === $post_type || 'page' === $post_type) ? false : $enabled;
}, 10, 2);

// Enable SVG file upload.
add_filter('upload_mimes', 'sk_cc_mime_types');
function sk_cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

// Enable 'editors' to edit menu navigation.
add_action('admin_head', 'sk_editor_menu');
function sk_editor_menu()
{
    remove_submenu_page('themes.php', 'themes.php');
    remove_submenu_page('themes.php', 'widgets.php');
    global $submenu;
    unset($submenu['themes.php'][6]);
}

// Show navigation (next/previous posts) when applicable.
if (!function_exists('sk_paging')) :
    function sk_paging()
    {
        global $wp_query;

        // Don't print empty markup if there's only one page.
        if ($wp_query->max_num_pages < 2)
            return;
?>

        <nav class="navigation paging-navigation" role="navigation">
            <h1 class="screen-reader-text">Posts navigation</h1>
            <div class="nav-links">

                <?php if (get_next_posts_link()) : ?>
                    <div class="nav-previous"><?php next_posts_link('<span class="meta-nav">&larr;</span> Older posts'); ?></div>
                <?php endif; ?>

                <?php if (get_previous_posts_link()) : ?>
                    <div class="nav-next"><?php previous_posts_link('Newer posts <span class="meta-nav">&rarr;</span>'); ?></div>
                <?php endif; ?>

            </div>
        </nav>
    <?php
    }
endif;


/**
 * AJAX
 */

add_action('wp_ajax_nopriv_some_action', 'ajax_cb');
add_action('wp_ajax_some_action', 'ajax_cb');
function ajax_cb()
{
    $action = $_POST['action'];

    $response = json_encode(array(
        'success' => true
    ));

    header('Content-Type: application/json');
    echo $response;

    exit;
}


/**
 * SETTINGS PAGE
 */

if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}


/**
 * CUSTOM POST TYPES
 */

// include_once 'cp/custom-post.php';
// include_once 'cp/news-post.php';
include_once 'cp/project-post.php';

global $site;
$site = new Site();

function SITE()
{
    global $site;
    return $site;
}

class Site
{

    function blocks($blocks)
    {
        foreach ($blocks as $block) {
            $this->block($block['acf_fc_layout'], $block);
        }
    }

    function block($layout, $data = null)
    {
        include __DIR__ . "/blocks/{$layout}.php";
    }

    function resource($file)
    {
        return get_template_directory_uri() . "/resources/{$file}";
    }

    function style($file)
    {
        return $this->resource("compiled/{$file}.min.css") . '?v=' . time();
    }

    function script($file)
    {
        return $this->resource("compiled/{$file}.min.js") . '?v=' . time();
    }

    function artwork($file)
    {
        return $this->resource("artwork/{$file}");
    }

    function placeholder($file)
    {
        return $this->resource("placeholders/{$file}");
    }

    function svg($file)
    {
        echo file_get_contents(__DIR__ . "/resources/artwork/{$file}.svg");
    }

    function svgpath($url)
    {
        echo file_get_contents($url);
    }

    function getMenuLevel($parent)
    {
        $items = wp_get_nav_menu_items('Main Menu');
        $filtered = [];
        foreach ($items as $item) {
            if ($item->menu_item_parent == $parent) {
                $filtered[] = $item;
            }
        }
        return $filtered;
    }
}

// add postype class name
add_filter('body_class', 'custom_posttype_class');
function custom_posttype_class($classes)
{
    global $post, $release;

    if (is_page_template('page-posttype.php')) {
        $posttype = get_field('post_type');
        $classes[] = 'posttype-' . $posttype;
    }
    if (is_page_template('page-standard.php')) {
        $page_slug = get_post_field('post_name');
        $classes[] = 'custompage-' . $page_slug;
    }
    if (is_singular('project')) {
        $p_hero = get_field('hero_block', $post->ID);
        $release['name'] = $p_hero['project_select'];
        $release['color'] = $release['name'] == 'stage-2' ? 'bg-orange' : 'bg-green';
        $classes[] = 'release-' . $release['name'];
    }
    return $classes;
}


function do_wrapChars($string)
{
    $string = strip_tags($string, '<em><strong>');
    $string_array = explode(' ', $string);
    $html = '';
    foreach ($string_array as $key => $s) {
        $html .= '<div style="position: relative; display: inline-block; margin-right:10px;">' . $s . '</div>';
    }
    return $html;
}


add_filter('acf/fields/google_map/api', function ($api) {
    $api['key'] = 'AIzaSyA3YBWI5Y4aGuFRi6wExC2LME7NuoRUD4c';
    return $api;
});

/**
 * get projects via category id
 */
function get_posts_by_cates($cat_id, $posts_per_page)
{
    $args = [];
    $post_type = 'project';
    if ($cat_id == 0) {
        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            //'offset' => $page * $posts_per_page
        );
    } else {
        $args = array(
            'post_type' => $post_type,
            'cat' => $cat_id,
            'posts_per_page' => $posts_per_page,
            //'offset' => $page * $posts_per_page
        );
    }
    //__l($args);
    $args['post_status'] = 'publish';
    $args['order'] = 'ASC';
    $query = new WP_Query($args);

    foreach ($query->posts as $key => $p) {
        $p_hero = get_field('hero_block', $p->ID);
        $image = get_the_post_thumbnail_url($p->ID, 'full');
        $results[] = (object) array(
            'id' => $p->ID,
            'title' => $p->post_title,
            'logo' => get_template_directory_uri() . '/resources/artwork/' . $p_hero['project_select'] . '-b.svg',
            'header' =>  $p_hero['header'],
            //'image' => $p_hero['background_image']['url'],
            'image' => $image,
            'content' => $p_hero['content'],
            'release' => $p_hero['project_select'],
            'url' => get_permalink($p->ID),
        );
    }

    return $results;
}

add_action('wp_footer', 'mycustom_wp_footer');
function mycustom_wp_footer()
{
    ?>
    <script type="text/javascript">
        document.addEventListener('wpcf7mailsent', function(event) {
            var fields = jQuery("form").serializeArray();
            jQuery.each(fields, function(i, field) {
                if (field.name == "apartment[]" && field.value == "DOWNLOAD APARTMENT BROCHURE") {
                    jQuery('.swl_custom_download').show();
                    jQuery('.swl_custom_download .apartment').show();
                }
                if (field.name == "apartment[]" && field.value == "DOWNLOAD TOWNHOUSE BROCHURE") {
                    jQuery('.swl_custom_download').show();
                    jQuery('.swl_custom_download .townhouse').show();
                }
            });
            //
        }, false);
    </script>
<?php
}
?>