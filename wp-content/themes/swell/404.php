<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content padding-top-240" role="main">

			<section class="b-padding md-outter">
				<header class="page-header margin-bottom-42">
					<h1 class="page-title"><?php __p('404: This page is useless'); ?></h1>
				</header>

				<div class="page-content">
					<p><?php __p('Please go back to <a href="'.get_home_url().'" >home</a>'); ?></p>
				</div>
			</section>

		</div>
	</div>

<?php get_footer(); ?>