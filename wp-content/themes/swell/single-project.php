<?php get_header();
global $release;
?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<?php while (have_posts()) : the_post(); ?>
			<?php
			$p_id = $post->ID;
			$blocks = get_field('blocks', $p_id);
			$p_feature_image = get_the_post_thumbnail_url($p_id);
			$p_hero = get_field('hero_block', $p_id);

			$release['name'] = $p_hero['project_select'];
			$release['color'] = $release['name'] == 'stage-2' ? 'bg-orange' : 'bg-green';
			//__l($release);
			?>
			<section class="hero_block b-relative border vh-100 ani ani-slide-up bg-load <?= $release['name'] ?> <?= empty($p_hero['background_image']) ? 'no-image' : ''; ?>" <?php if (!empty($p_hero['background_image'])) : ?> data-bg-load="<?= $p_hero['background_image']['url'] ?>" <?php endif; ?>>
				<div class="outter">
					<div class="intro ani ani-fade-in">
						<h5><?= $p_hero['header']; ?></h5>
						<?php if ($release['name'] == 'stage-3') : ?>
							<h1>STAGE THREE
							</h1>
						<?php else : ?>
							<img src="<?php __a('resources/artwork/' . $release['name'] . '.svg')  ?>" alt="image" />
						<?php endif; ?>
						<?= $p_hero['content']; ?>
					</div>
				</div>
			</section>

			<section class="anchor-menu border">
				<div class="outter">
					<div class="flex-box-2">
						<div class="l-box">
							<h3><a href="/">NV</a></h3>
						</div>
						<div class="r-box">
							<div class="logo">
								<?php if ($release['name'] == 'stage-3') : ?>
									<h3>STAGE THREE
									</h3>
								<?php else : ?>
									<img src="<?php __a('resources/artwork/' . $release['name'] . '-b.svg')  ?>" height="22" alt="image" />
								<?php endif; ?>
							</div>
							<div class="menu">
								<ul class="block-anchor">
									<?php foreach ($blocks as $key => $b) : ?>
										<?php if (!empty($b['id'])) : ?>
											<li><a href="#<?= strtolower($b['id']) ?>"><?= ucfirst($b['id']) ?></a></li>
										<?php endif; ?>
									<?php endforeach; ?>
								</ul>
								<ul class="">
									<li><a href="#register">Register</a></li>
									<li class="button"><a class="menu font-nv-bold" href="#register">MENU</a></li>
								</ul>
							</div>
						</div>
					</div>

				</div>
			</section>
			<?php
			SITE()->blocks($blocks);
			?>

		<?php endwhile; ?>

	</div>
</div>

<?php get_footer(); ?>