var Base, jQuery, Rellax, Swiper, timeLine, releases;

var $window = $(window),
$html = $('html, body'),
$body = $('body'),
$popbg = $('.popup-bg'),
activeCls = 'active',
clsPop = 'popup',
$pageLoading = $('.page-loading'),
//$formWrap = $('.form-wrap'),
$menuWrap = $('.menu-wrap'),
actionTime = 0.4,
delayTime = 0.4;
$mview = $('.mview'),
$anchorMenu = $('.anchor-menu'),
position = 0;
    

(function ( $, window, document ){

	'use strict';

	/**
	 * object for namespacing theme functions.
	 */

	Base = {

		/**
		* global variables.
		*/
		scrollTop: 0,
		windowHeight: 0,

		/**
		* initialiser.
		*/

		 init: function() {
			// Init VH
			Base.vh(); 
			Base.browserCompat();
			Base.load();
			Base.bindEvents();
			Base.ani();
			//Base.parallax();
			Base.initForm();
			//Base.menu();
			Base.gallery();
			Base.mapFilter();
		 },

		_viewIsMobile: function() {
			return !!$mview.is(':visible');
		},

		/**
		 * init vh css variable so it works on mobile
		 */

		vh: function() {
			var vh = $window.height() * 0.01;
			document.documentElement.style.setProperty('--vh', vh + 'px');
		},
	  
		/**
		 * detect and show warning for old versions of IE
		 */
	  
		browserCompat: function() {
			if (/Trident\/|MSIE/.test(window.navigator.userAgent)) {
				$body.append('<div class="browsehappy">Your browser is ancient. <a href="https://browsehappy.com/" target="_blank">Upgrade to a modern browser</a> to properly view this and other modern websites.');
			}
		},

		load: function() {
			Base.windowHeight = $window.height();
			$('.bg-load').each(function() {
				var $frame = $(this),
				imageSrc = $frame.attr('data-bg-load');
				//console.log(imageSrc);
				if(imageSrc && imageSrc !== ''){
					var image = new window.Image();
					//console.log(image);
					image.onload = function() {
						$frame.css('background-image', 'url(' + imageSrc + ')');
						$frame.removeAttr('data-bg-load');
					};
					image.src = imageSrc;
				}
			});

			$('.lazy-loading').each(function() {
				var $frame = $(this);
				var imageSrc = $(this).attr('data-src');
				if(imageSrc && imageSrc !== ''){
					var image = new window.Image();
					image.onload = function() {
						$frame.attr('src',imageSrc);
						$frame.removeAttr('data-src');
					};
					image.src = imageSrc;
				}
			});
		},

		ani: function() {
			var delay = 500;
			var fastDelay = 250;
			$('.ani-group').each(function(){
				var $this = $(this);
				delay = 500;
				$this.find('.ani').not('.ani-go').each( function(idx) {
					$(this).css({transitionDelay: delay+'ms'});
					delay += 200;
				});
			});

			$('.ani-group-fast .ani').not('.ani-go').each( function() {
				$(this).css({transitionDelay: fastDelay+'ms'});
				fastDelay += 50;
			});
			$('.ani:in-viewport').addClass('ani-go');

			$('.svg-ani:in-viewport').each(function(){
				var $this = $(this),
				svg = $this.find('#lottie'),
				path = $this.data('path'),
				animation = window.bodymovin.loadAnimation({
					container : svg.get(0),
					renderer : 'svg',
					loop : false,
					autoplay : true,
					path : path
				});
				//animation.goToAndStop(14, true);
                
				$this.on('click', function() {
					animation.playSegments([0, 90], true);
				});
			});
			$('.gsap-word-ani').each(function(){
				var $this = $(this),
					tl = gsap.timeline(), 

				chars = $this.find('div'); //an array of all the divs that wrap each character
				gsap.set(chars, {perspective: 400});
				chars.each(function(idx, el){
					tl.fromTo(el, {opacity: 0, rotationX : -50, transformOrigin : 'left bottom', ease : window.Power2.easeOut }, {opacity: 1, rotationX: 0, duration:0.1, stagger: 0.01},"+=0.01") 
					;
					
				});
				// for (var i = 1; i < chars.length; i++) {
				// 	//tl.from(chars[i], 1, { xPercent:100, ease: Linear.easeNone }, "+=1");
				// 	tl.fromTo(chars[i], {opacity: 0, rotationX : -50, transformOrigin : 'left bottom', ease : Elastic.easeInOut }, {opacity: 1, rotationX: 0, duration:0.1},"");
				//   }
				
				//tl.from(chars, {duration: 0.8, opacity:0, scale:0, y:80, rotationX:180, transformOrigin:"0% 50% -50",  ease:"back", stagger: 0.01}, "+=0")
				
			});
		},

		menu:function() {
			if($body.hasClass('single-news')){
				$('#site-navigation li.news').addClass('current_page_item');
			}
			if($body.hasClass('single-project')){
				$('#site-navigation li.projects').addClass('current_page_item');
			}
		},

		gallery:function(){
			$('.gallery_block').each(function(){
				var $swiper = $(this).find('.swiper-container');
				var swiper = new Swiper($swiper, {
					spaceBetween: 0,
					speed: 1000,
					effect: 'fade',
					autoplay: {
						delay: 2000,
						disableOnInteraction: false,
					},
					// navigation: {
					// 	nextEl: $swiper.find('.swiper-button-next'),
					// 	prevEl: $swiper.find('.swiper-button-prev'),
					// },
					pagination: {
						el: $swiper.find('.swiper-pagination'),
						clickable: true,
						renderBullet: function (index, className) {
						  return '<span class="' + className + '"></span>';
						},
					},
				});
			});
			$('.content_slider_block').each(function(){
				var $carousel = $(this).find('.swiper-carousel');
				var swiper = new Swiper($carousel, {
					spaceBetween: 0,
					speed: 500,
					effect: 'fade',
					fadeEffect: {
						crossFade: true
					},
					// autoplay: {
					// 	delay: 2000000,
					// 	disableOnInteraction: true,
					// },
					pagination: {
						el: $carousel.find('.swiper-pagination'),
						clickable: true,
						renderBullet: function (index, className) {
							var caption = $carousel.find('.swiper-slide[data-swiper-slide-index="'+ index +'"]').attr('data-subtitle');
							return '<li class="' + className + '">' + caption + '<svg xmlns="http://www.w3.org/2000/svg" width="139.444" height="96.46" viewBox="0 0 139.444 96.46"><path id="Path_4" data-name="Path 4" d="M48.277-123.508a36.125,36.125,0,0,0-10.9,7.332A85.57,85.57,0,0,0,14.281-82.27c-2.772,7.725-4.2,15.818-3.042,23.515A29.152,29.152,0,0,0,22.967-39.061c5.469,3.865,12.495,5.842,19.641,6.878,23.6,3.422,49.96-3.272,69.587-17.673,13.365-9.807,23.8-23.508,25.918-38.084s-5.277-29.67-19.83-35.423c-9.036-3.572-19.828-3.4-30.116-1.707a113.068,113.068,0,0,0-48.657,20.6C25.458-93.992,14.458-80.287,8.151-65.4,4.638-57.1,2.57-48.517.7-39.958" transform="translate(0.196 126.988)" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg></li>';
						},
					},
					on: {
						init: function () {
        
						},
						transitionStart: function(swiper) {
							var svg = $carousel.find('.swiper-slide-active .svg-ani-static'),
							path = svg.data('path'),
							lottie = $carousel.find('.swiper-slide-active #lottie'),
							animation = window.bodymovin.loadAnimation({
								container : lottie.get(0),
								renderer : 'svg',
								loop : false,
								autoplay : true,
								path : path
							});
							//animation.play();
							// animation.goToAndStop(1000, true);
							// animation.playSegments([0, 1000], true);
							animation.goToAndStop(0, true);
							svg.on('click', function() {
								animation.playSegments([0, 90], true);
							});
						},
						transitionEnd: function(swiper) {
						 
						},
						slideChange: function () {
						

						},
					},
				});
			});
		},

		bindEvents: function() {
			$window.resize(function() {
				Base.windowHeight = $window.height();
				Base.vh();
				Base.ani();
				// Base.masthead();
			});
		
			$window.scroll(function() {
				Base.ani();

				var prevScrollTop = Base.scrollTop;
				Base.scrollTop = $(document).scrollTop();
		
				// Detect scroll direction
				if (prevScrollTop > Base.scrollTop) {
				  $body.addClass('scrolling-up').removeClass('scrolling-down');
				} else if ( prevScrollTop < Base.scrollTop ) {
				  $body.addClass('scrolling-down').removeClass('scrolling-up');
				}
		
				// Detect scrolled
				if (Base.scrollTop > 10) {
				  $body.addClass('scrolled');
				} else {
				  $body.removeClass('scrolled scrolling-down scrolling-up');
				}
		
				// Detect scrolled past 100vh
				if (Base.scrollTop > Base.windowHeight) {
				  $body.addClass('scrolled-fold');
				} else {
				  $body.removeClass('scrolled-fold');
				}
			});

			

			// anchor click
			$(document).on('click', 'a[href^="#"]', function (event) {
				event.preventDefault();
				var $target = $($.attr(this, 'href'));
				if($target.length){
					$html.animate({
						scrollTop: $target.offset().top
					}, 500);
				}
			});


			$('.button >a.menu').on('click', function(e){
				e.preventDefault();
				position = $body.hasClass('single-project')?$('.anchor-menu').offset().top : $('#masterhead').offset().top;
				timeLine = window.TweenMax;
				timeLine.fromTo($pageLoading,{
					x : '0',
				},{
					ease : Expo.linear, 
					x : '-100vw',
					duration: actionTime,
					//delay: delayTime,
					onComplete: function(){
						//that.ani();
						$html.animate({ scrollTop : 0 }, 0);
					}
				},'mark'); 
				timeLine.fromTo($menuWrap,{
					x : '100vw', 
				},{
					ease : window.Power2.easeOut, 
					x : '0',
					duration: actionTime,
					//delay: delayTime,
					onComplete: function(){
						// TweenMax.set( $menuWrap, {
						// 	clearProps : 'all'
						// });
						$body.addClass('menu-show');
					}
				},'mark'); 
				
			});

			$('.button >a.close').on('click', function(e){
				e.preventDefault();
				$body.removeClass('menu-show');
						$html.scrollTop(position);
				timeLine = window.TweenMax;
				timeLine.fromTo($menuWrap,{
					x : '0',
				},{
					ease : window.Power2.easeOut, 
					x : '100vw',
					duration: actionTime,
					//delay: delayTime,
					onComplete: function(){
						
						// TweenMax.set( $menuWrap, {
						// 	clearProps : 'all'
						// });
					}
				},'mark'); 
				timeLine.fromTo($pageLoading,{
					x : '-100vw',
				},{
					ease : Expo.linear, 
					x : '0',
					duration: actionTime,
					//delay: delayTime,
					onComplete: function(){
						//that.ani();					
						
						TweenMax.set( $pageLoading, {
							clearProps : 'all'
						});
						
					}
				},'mark'); 
				
				
			});
			


			$('.video_block').each(function(){
				var $this = $(this),
				video = $this.data('video');
				$this.on('click', function(e){
					var $box = $popbg.find('.popup-inner');
					e.preventDefault();
					$box.html(video);
					$popbg.addClass(clsPop);
					$body.addClass(clsPop);
					$('.vimeo').on('load', function(){
						timeLine = window.TweenMax;
						timeLine.fromTo($box.addClass(activeCls),{
							y : '-150px',
						},{
							ease : window.Power3.easeOut, 
							y : '0',
							duration: 0.7,
							delay: 0.3,
							// onComplete: function(){
							// 	//that.ani();
							// 	$html.animate({ scrollTop : 0 }, 0);
							// }
						}); 
					});
				});
			});

			$body.on('click', '.popup-bg.popup:not(.popup-window)', function(e){
				e.preventDefault();
				$popbg.find('.popup-inner').html('');
				$popbg.removeClass(clsPop);
				$body.removeClass(clsPop);
				$box.removeClass(activeCls)
			});


			$('.floorplan_block').each(function(){
				var $el = $(this);
				var $parent = $el.find('.swl-select');
				$parent.on('click', function(e){
					e.preventDefault();
					var $this = $(this);
					if( ! $this.hasClass('open') ) {
						$this.addClass('open');
					}else{
						$parent.removeClass('open');
					}
				});
			});
			
		},

		// parallax animation
		parallax: function(){
			if ( $('.rellax').length ) {
				new Rellax('.rellax' );  
			}  
		},

		//map filter
		mapFilter: function(){
			$('.map_filter_block').each(function() {

				var $this = $(this);

					// var
					var poi_map = $this.find('#poi-map');
					var pois = $this.find('#pois .poi');
					if (poi_map.length === 0) {
					  return;
					}
			  
					var zoom = 16;
					var mapStyle = [{
						"elementType": "geometry",
						"stylers": [{
						  "color": "#f5f5f5"
						}]
					  },
					  {
						"elementType": "labels.icon",
						"stylers": [{
						  "visibility": "off"
						}]
					  },
					  {
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#616161"
						}]
					  },
					  {
						"elementType": "labels.text.stroke",
						"stylers": [{
						  "color": "#f5f5f5"
						}]
					  },
					  {
						"featureType": "administrative.land_parcel",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#bdbdbd"
						}]
					  },
					  {
						"featureType": "poi",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#eeeeee"
						}]
					  },
					  {
						"featureType": "poi",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#757575"
						}]
					  },
					  {
						"featureType": "poi.park",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#e5e5e5"
						}]
					  },
					  {
						"featureType": "poi.park",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#9e9e9e"
						}]
					  },
					  {
						"featureType": "road",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#ffffff"
						}]
					  },
					  {
						"featureType": "road.arterial",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#757575"
						}]
					  },
					  {
						"featureType": "road.highway",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#dadada"
						}]
					  },
					  {
						"featureType": "road.highway",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#616161"
						}]
					  },
					  {
						"featureType": "road.local",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#9e9e9e"
						}]
					  },
					  {
						"featureType": "transit.line",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#e5e5e5"
						}]
					  },
					  {
						"featureType": "transit.station",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#eeeeee"
						}]
					  },
					  {
						"featureType": "water",
						"elementType": "geometry",
						"stylers": [{
						  "color": "#c9c9c9"
						}]
					  },
					  {
						"featureType": "water",
						"elementType": "labels.text.fill",
						"stylers": [{
						  "color": "#9e9e9e"
						}]
					  }
					];
					
					var foundslaneImg = window.__APP.templateUrl + '/resources/artwork/map/'+ $this.data('marker') +'-marker.svg';
					// if ($('body').is('.postid-449, .postid-666')) {
					//   mapStyle = Founders.map_style_new;
					//   foundslaneImg = SwellBaseUrl + "/assets/img/map/founderslane-gold.png";
					// }
			  
					var gmap = new google.maps.Map(document.getElementById('poi-gmap'), {
					  disableDefaultUI: true,
					  zoom: zoom,
					  minZoom: zoom,
					  maxZoom: zoom,
					  styles: mapStyle
					});
			  
					var hero_marker = new google.maps.Marker({
					  position: {
						lat: -35.276728,
						lng: 149.135922
					  },
					  map: gmap,
					  icon: {
						url: foundslaneImg,
						scaledSize: new google.maps.Size(111, 135),
						origin: new google.maps.Point(0, 0),
						anchor: new google.maps.Point(61, 0),
					  }
					});
			  
					gmap.setCenter(hero_marker.getPosition());
			  
					var markers = [];
					var infowindows = [];
					var icons = {};
					var categories = ['restaurant', 'cafe', 'entertainment', 'culture', 'lifestyle', 'shopping', 'transport', 'school'];
			  
					// init icons
					$.each(categories, function(i, category) {
					  icons[category] = {
						url: window.__APP.templateUrl + '/resources/artwork/map/' + category + '.svg'
					  }
					});
			  
					// init active states for icons
					$.each(icons, function(key, icon) {
					  icon.scaledSize = new google.maps.Size(30, 30);
					  var new_icon = $.extend(true, {}, icon);
					  icons[key] = icon;
					  icons[key + '_active'] = new_icon;
					  icons[key + '_active'].url = new_icon.url.replace('.png', '-active.svg');
					});
			  
					function _closeInfoWindows() {
					  $.each(infowindows, function(i, infowindow) {
						//infowindow.close();
					  });
					  $.each(markers, function(i, m) {
						m.setIcon(icons[m.cat]);
					  });
					}
			  
					google.maps.event.addListener(gmap, "click", function() {
					  _closeInfoWindows();
					});
			  
					pois.each(function() {
					  var poi = $(this);
					  var text = poi.html();
					  var lat = parseFloat(poi.attr('data-lat'));
					  var lng = parseFloat(poi.attr('data-lng'));
					  var cat = poi.attr('data-cat');
			  
					  var marker = new google.maps.Marker({
						position: {
						  lat: lat,
						  lng: lng
						},
						map: gmap,
						cat: cat,
						icon: icons[cat]
					  });
			  
					  var infowindow = new google.maps.InfoWindow({
						content: text
					  });
			  
					  marker.addListener('click', function() {
						if (infowindow.getMap() !== null && typeof infowindow.getMap() !== "undefined") {
						  infowindow.close();
						  marker.setIcon(icons[cat]);
						} else {
						  _closeInfoWindows();
						  infowindow.open(gmap, marker);
						  marker.setIcon(icons[cat + '_active']);
						}
					  });
			  
					  // Add info window classes
					  google.maps.event.addListener(infowindow, 'domready', function() {
						var content, window, close, arrow;
						content = $('.gm-style-iw');
						window = content.parent().addClass('info-window-background');
						close = content.next().addClass('info-window-close');
						arrow = window.children().first().addClass('info-window-arrow')
					  });
			  
					  markers.push(marker);
					  infowindows.push(infowindow);
					});
			  
					// Init POI Map Filters for mobile and desktop
					var filters = $this.find('#poi-filter ul.ul-filter a');
					var select = $this.find('#poi-filter-select');
			  
					//pc change filter
					select.change(function() {
					  var active = $(this).val();
					  if (active === "") {
						$.each(markers, function(i, m) {
						  m.setMap(gmap);
						});
					  } else {
						$.each(markers, function(i, m) {
						  if (active.indexOf(m.cat) !== -1) {
							m.setMap(gmap);
						  } else {
							m.setMap(null);
						  }
						});
					  }
					});
			  
					filters.click(function(event) {
					  event.preventDefault();
					  var filter = $(this);
					  filter.blur();
					  filter.toggleClass('active');
					  if (filter.hasClass('active')) {
						filters.not(filter).removeClass('active');
						var active = filter.attr('data-poi-filter');
						select.val(active).change();
					  } else {
						select.val("").change();
					  }
			  
					});
		
			  });
		},

		// change contact form 7 submit button layout
		initForm: function(){
			var btn = $('.wpcf7-form input[type="submit"]');
			if ( btn.length ) {
				btn.replaceWith('<button type="submit" class="' + btn.attr('class') + '">' + btn.val() + '</button>');
			}
			Base.initCheckboxes();
			Base.initSelects()
			Base._MaterialFormLabels();
			Base._RegisterReleasePicker();
		},

	/**
	 * Add focus class for sweet material label behaviour
	 */

		_MaterialFormLabels: function() {

			var material_inputs_selector = '.form-group input, .form-group textarea, .form-group select';
			var material_inputs = $(material_inputs_selector);

			// Keep listening for browser autofill
			var interval_total = 0;
			var interval = setInterval(function() {
				material_inputs = $(material_inputs_selector);
				material_inputs.each(function() {
					var elem = $(this);
					if (elem.val()) {
						elem.change().trigger('blur');
					  }
				});
				interval_total += 250;
				if ( interval_total > 1000 ) {
					clearInterval(interval);
				}
			}, 250);

			$('body').on('focus', material_inputs_selector, function() {
				$(this).parents('.form-group').addClass('non-empty').addClass('focused');
			});

			$('body').on('blur', material_inputs_selector, function() {
				var parent = $(this).parents('.form-group');
				if ( $(this).val() ) {
					parent.addClass('non-empty');
				} else {
					parent.removeClass('non-empty');
				}
				parent.removeClass('focused');
			});

			$('body').on('change', material_inputs_selector, function() {
				$(this).blur();
			});

		},

	/**
	 * Init styled radio buttons that appear on the Register form when selecting a release
	 */

		_RegisterReleasePicker: function() {
			var input = $('input#release');
			// if ( input.length === 0 || releases.length === 0 ) { return; }

			// var control = $('<div class="swl-releases"></div>');
			// input.after(control);

			// $.each(releases, function(key,release) {
			// 	var logos = '<span class="sr-only">' + release.title + '</span>';
			// 	logos += '<span class="swl-release-logo swl-release-logo-white" style="background-image:url(' + release.logo_white + ');"></span>';
			// 	logos += '<span class="swl-release-logo swl-release-logo-black" style="background-image:url(' + release.logo_black + ');"></span>';
			// 	control.append('<a class="swl-release" href="#" data-title="' + release.title + '">' + logos + '</a>');
			// });

			$('a.swl-release').click(function(e) {
				e.preventDefault();
				$(this).toggleClass('checked').blur();
				var val = '';
				$('a.swl-release.checked').each(function() {
					val += ( val ? ', ' : '' ) + $(this).attr('data-title');
				});
				input.val(val);
			});

			// Pre-fill release specific forms
			if ( release ) {
				input.val(release).parents('.form-group').hide();
			}
		},

		/**
	 * Initialise checkboxes
	 */

		 initCheckboxes: function() {
			// Iterate through all the checkboxes
			$('input[type=checkbox]:not(:hidden)').each(function() {
				// Hide the actual checkbox
				var input = $(this).hide();

				// Create the DOM structure
				var el = $('<a></a>').addClass('swl-checkbox').attr({ href: '#' }).insertAfter(input);

				// Apply the same tab index
				if( input.attr('tabindex') ) {
					el.attr({ tabindex: input.attr('tabindex') });
				}

				// Grab some details to pass to the event handlers
				var widget = { el: el, input: input };

				// Sync the state of the two elements.
				input.on('change swl-checkbox-change',widget,function(e) {
					if( $(this).is(':checked') ) {
						e.data.el.addClass('checked');
					}
					else {
						e.data.el.removeClass('checked');
					}
				}).trigger('swl-checkbox-change');

				// When our element is clicked, toggle the checkbox.
				el.click(widget,function(e) {
					e.preventDefault();
					$.swell.controls._handleCheckboxToggle( e.data.el, e.data.input );
				});

				// When our element is clicked, toggle the checkbox.
				el.keypress(widget,function(e) {
					if( e.which === 13 ) {
						$.swell.controls._handleCheckboxToggle( e.data.el, e.data.input );
					}
				});

			});
		},

	/**
	 * Function to update checkbox state when pseudo checkbox is triggered.
	 */

		_handleCheckboxToggle: function( el, input ) {
			if ( input.hasClass('wpcf7-form-control') ) {
				input.click();		
			} else {
				input.prop({ checked: ! el.hasClass('checked') }).change();
			}			
		},

	/**
	 * Initialise selects
	 */

		initSelects: function() {
			// Iterate through all the checkboxes
			$('select:not([multiple]):not(:hidden)').each(function() {
				// Hide the actual checkbox
				var select = $(this).hide();

				// Create the DOM wrapper				
				select.wrap('<div class="swl-select-wrapper"></div>');
				
				// Create the select wrapper
				var el = $('<a></a>').addClass('swl-select').attr({ href: '#' }).insertBefore(select);

				// Apply the same tab index
				if( select.attr('tabindex') ) {
					el.attr({ tabindex: select.attr('tabindex') });
				}

				// Create an unordered list for options
				var ul = $('<ul></ul>').addClass('swl-select-items');
				select.children().each(function() {
					ul.append( '<li class="unselected" data-value="' + this.value + '">' + this.text + '</li>' );
				});

				// Drop the DOM into place.
				ul.appendTo(el);

				// Grab some details to pass to the event handlers
				var widget = { el: el, ul: ul, select: select };

				// Sync the state of the two elements.
				select.on('change swl-select-change',widget,function(e) {
					var value = e.data.select.val();
					var li = e.data.ul.find('li[data-value="'+value+'"]').removeClass('unselected').addClass('selected');
					e.data.ul.find('li.selected').not(li).removeClass('selected').addClass('unselected');
				}).trigger('swl-select-change');

				// Open the dropdown on click
				el.click(widget,function(e){
					e.preventDefault();

					// If closed, open the list.
					if( ! e.data.el.hasClass('open') ) {
						e.data.el.addClass('open');
					}
				});

				// Open the dropdown on click
				ul.children().click(widget,function(e){
					e.preventDefault();

					// If the list isn't open, do nothing.
					if( ! e.data.el.hasClass('open') ) {
						return;
					}

					// Don't propogate
					e.stopPropagation();

					// Set the value
					e.data.select.val( $(this).attr('data-value') ).change();

					// Close the list
					e.data.el.removeClass('open');
				});

			});

			// Clicking anywhere in the document
			$('html').click(function(e) {
				var $target = $(e.target);

				// If this a child of a select, close other selects.
				if( $target.parents('.swl-select').length > 0 ) {
					$('.swl-select.open').not( $target.parents('.swl-select') ).removeClass('open');
				}

				// If this a select, close other selects.
				else if( $target.hasClass('swl-select') ) {
					$('.swl-select.open').not( $target ).removeClass('open');
				}

				// If this is not a select, close open selects.
				else {
					$('.swl-select.open').removeClass('open');
				}
			});

		}

	}

	$(document).ready(function() {
		Base.init();
		$body.addClass('document-ready');
		$(window).trigger('resize').trigger('scroll');
	});
	  
	// $window.load(function() {
	// 	$body.addClass('window-load');    
	// 	$(window).trigger('resize').trigger('scroll');
	// });
	
})(jQuery, window, document);

