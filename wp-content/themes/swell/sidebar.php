<aside class="sidebar-container">
	<div class="widget-area">
		<div class="widget">
			<h4>Archives</h4>
			<ul>
				<?php wp_get_archives('type=monthly'); ?>
			</ul>
		</div>

		<div class="widget">
			<h4>Categories</h4>
			<ul>
				<?php wp_list_categories('show_count=1&title_li='); ?>
			</ul>
		</div>

		<?php if (is_active_sidebar('sidebar-1')) : ?>
			<?php dynamic_sidebar('sidebar-1'); ?>
		<?php endif; ?>
	</div>
</aside>