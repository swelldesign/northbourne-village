</div>
<section id="register" class="form_block b-relative border bg-black ani ani-slide-up">
    <div class="outter">
        <div class="flex-box-2">
            <div class="l-box">
                <h2>Register<br />your<br />Interest</h2>
            </div>
            <div class="r-box">
                <?= do_shortcode('[contact-form-7 id="69" title="Contact form 1"]'); ?>
            </div>
        </div>
    </div>
</section>
<div class="popup-bg">
    <a class="popup-close" href="#"><img src="<?php __a('resources/artwork/icon-close.svg'); ?>" width="18" height="18" /></a>
    <div class="popup-window">
        <div class="popup-inner">

        </div>
    </div>
</div>
<footer id="masterfoot" class="site-footer b-relative" role="contentinfo">
    <div class="footer-top bg-grey">
        <div class="outter">
            <div class="flex-box-right-2">
                <div class="l-box">
                    <div class="flex-column">
                        <div class="box"><a href="<?php __p(esc_url(home_url('/'))); ?>" title="Home"><img src="<?php __a('/resources/artwork/logo-jw.svg') ?>" /></a></div>
                        <div class="box">
                            <p>Northbourne Village is a celebration of local founders and makers and JWLand is proud to support Canberra businesses through the delivery of this project.</p>
                        </div>
                    </div>
                </div>
                <div class="r-box">
                    <h5> Visit the Sales Suite</h5>
                    <p>59 Currong Street North, Braddon<br />
                        Friday to Tuesday from 10am - 3pm</p>
                    <h5>Book a Private Appointment</h5>
                    <p>Call Sarah on 0448 249 199,<br />
                        Natasha on 0434 104 556,<br />
                        or email <a href="mailto:sales@founderslane.com.au">sales@founderslane.com.au</a></p>
                    <h5> Visit the Sales Suite</h5>
                    <ul>
                        <li>
                            <a target="_blank" href="https://www.instagram.com/jwland_cbr/">Instagram</a>
                        </li>
                        <li><a target="_blank" href="https://www.facebook.com/jwlandgroup">Facebook</a></li>
                        <li><a target="_blank" href="https://www.youtube.com/channel/UCw7U16MFEh6dO11Abb-UW5g">Youtube</a></li>
                    </ul>


                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="outter">
            <div class="flex-box-right-2">
                <div class="l-box">
                    <?php
                    wp_nav_menu(array(
                        'container' => false,
                        'container_class' => 'menu clearfix',
                        'theme_location' => 'main-nav'
                    ));
                    ?>
                </div>
                <div class="r-box">
                    <div class="swell">
                        <a href="https://swelldesigngroup.com/" title="Site by Swell" target="_blank">Site by Swell</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</div><!-- close page loading -->
<span class="mview"></span>

<script type="text/javascript">
    (function() {
        var APP = {};
        APP.templateUrl = '<?php echo get_bloginfo("template_url"); ?>';
        APP.ajaxUrl = '<?php echo admin_url("admin-ajax.php"); ?>';
        APP.isRetina = (window.devicePixelRatio > 1 || (window.matchMedia && window.matchMedia('(-webkit-min-device-pixel-ratio: 1.5),(-moz-min-device-pixel-ratio: 1.5),(min-device-pixel-ratio: 1.5)').matches));
        APP.isSurfaceDevice = (function() {
            var bool = false;
            try {
                var msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture;
                if (('ontouchstart' in window) || msGesture || window.DocumentTouch && document instanceof DocumentTouch) {
                    bool = true;
                    document.getElementsByTagName('html')[0].className += ' ua-surface';
                }
            } catch (e) {}
            return bool;
        })();
        window.__APP = APP;
    })();
</script>

<script src="<?= SITE()->script('script'); ?>"></script>


</html>