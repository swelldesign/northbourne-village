<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php if (have_posts()) : ?>
				<header class="page-header">
					<h1 class="page-title"><?php printf('Search Results for: %s', get_search_query()); ?></h1>
				</header>

				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('content'); ?>
				<?php endwhile; ?>

				<?php sk_paging(); ?>
			<?php else : ?>
				<?php get_template_part('content', 'none'); ?>
			<?php endif; ?>

		</div>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>