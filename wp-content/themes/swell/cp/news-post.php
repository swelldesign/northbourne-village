<?php
// DEPRECATED. Use ACF Pro instead.
// include TEMPLATEPATH . '/metaboxes/custom-post-spec.php';

add_action('init', 'sk_register_cp_news');
function sk_register_cp_news() {
    register_post_type('news', array(
        'labels' => array(
            'name' => 'News',
            'singular_name' => 'News Post',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New News Post',
            'edit' => 'Edit',
            'edit_item' => 'Edit News Post',
            'new_item' => 'New News Post',
            'view' => 'View',
            'view_item' => 'View News Post',
            'search_items' => 'Search News Posts',
            'not_found' => 'No News Posts found',
            'not_found_in_trash' => 'No News Posts found in Trash',
            'parent' => 'Parent News Post'
        ),
        'exclude_from_search' => true,
        'publicly_queryable' => true,//remove single post url
        'hierarchical' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-category',
        'supports' => array('title', 'editor'),
        'taxonomies' => array(''),
        'has_archive' => false,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
        'rewrite' => array(
            'slug' => 'news',
            'with_front' => false
        ),
        'public' => true
    ));
    flush_rewrite_rules();
}