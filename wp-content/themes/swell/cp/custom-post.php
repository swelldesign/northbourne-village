<?php
// DEPRECATED. Use ACF Pro instead.
// include TEMPLATEPATH . '/metaboxes/custom-post-spec.php';

add_action('init', 'sk_register_cp_custompost');
function sk_register_cp_custompost() {
    register_post_type('custompost', array(
        'labels' => array(
            'name' => 'Custom Posts',
            'singular_name' => 'Custom Post',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Custom Post',
            'edit' => 'Edit',
            'edit_item' => 'Edit Custom Post',
            'new_item' => 'New Custom Post',
            'view' => 'View',
            'view_item' => 'View Custom Post',
            'search_items' => 'Search Custom Posts',
            'not_found' => 'No Custom Posts found',
            'not_found_in_trash' => 'No Custom Posts found in Trash',
            'parent' => 'Parent Custom Post'
        ),
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'hierarchical' => true,
        'menu_position' => 30,
        'menu_icon' => 'dashicons-category',
        'supports' => array('title', 'editor'),
        'taxonomies' => array(''),
        'has_archive' => false,
        'rewrite' => array(
            'slug' => 'custompost',
            'with_front' => false
        ),
        'public' => true
    ));
    flush_rewrite_rules();
}