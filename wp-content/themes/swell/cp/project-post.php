<?php
// DEPRECATED. Use ACF Pro instead.
// include TEMPLATEPATH . '/metaboxes/custom-post-spec.php';

add_action('init', 'sk_register_cp_project');
function sk_register_cp_project() {
    register_post_type('project', array(
        'labels' => array(
            'name' => 'Project',
            'singular_name' => 'Project Post',
            'add_new' => 'Add New',
            'add_new_item' => 'Add New Project Post',
            'edit' => 'Edit',
            'edit_item' => 'Edit Project Post',
            'new_item' => 'New Project Post',
            'view' => 'View',
            'view_item' => 'View Project Post',
            'search_items' => 'Search Project Posts',
            'not_found' => 'No Project Posts found',
            'not_found_in_trash' => 'No Project Posts found in Trash',
            'parent' => 'Parent Project Post'
        ),
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'hierarchical' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-category',
        'supports' => array('title', 'editor'),
        'taxonomies' => array(''),
        'has_archive' => false,
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
        'rewrite' => array(
            'slug' => 'project',
            'with_front' => false
        ),
        'public' => true
    ));
    flush_rewrite_rules();
}