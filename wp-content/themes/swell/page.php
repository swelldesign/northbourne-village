<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if (has_post_thumbnail() && !post_password_required()) : ?>
							<div class="entry-thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
						<?php endif; ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header>

					<div class="entry-content">
						<?php
						the_content();
						wp_link_pages();
						?>
					</div>

					<footer class="entry-meta">
						<?php edit_post_link(__p('Edit'), '<span class="edit-link">', '</span>'); ?>
					</footer>
				</article>

				<?php comments_template(); ?>

			<?php endwhile; ?>

		</div>
	</div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>