<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php wp_title('|', true, 'right'); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-tap-highlight" content="no">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php __a('resources/artwork/favicon/apple-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php __a('resources/artwork/favicon/apple-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php __a('resources/artwork/favicon/apple-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php __a('resources/artwork/favicon/apple-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php __a('resources/artwork/favicon/apple-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php __a('resources/artwork/favicon/apple-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php __a('resources/artwork/favicon/apple-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php __a('resources/artwork/favicon/apple-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php __a('resources/artwork/favicon/apple-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php __a('resources/artwork/favicon/android-icon-192x192.png'); ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php __a('resources/artwork/favicon/favicon-32x32.png'); ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php __a('resources/artwork/favicon/favicon-96x96.png'); ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php __a('resources/artwork/favicon/favicon-16x16.png'); ?>">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php __a('/ms-icon-144x144.png'); ?>">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://use.typekit.net/kle6yfd.css">


    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <link rel="stylesheet" href="<?= SITE()->style('style'); ?>">
    <script src="<?php __a('ani/data.js'); ?>"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <aside class="menu-wrap">
        <div class="outter">
            <div class="flex-box-2">
                <div class="l-box">

                    <div class="top-box box">
                        <h5 class="header">NOW SELLING</h5>
                        <div class="spot">
                            <a href="<?php __p(esc_url(home_url('/project/embark/'))); ?>"><img class="stage-1" src="<?php __a('/resources/artwork/logo-1.svg') ?>" /></a>
                            <div class="info">1, 2 and 3 bedrooom apartments.</div>
                        </div>
                        <div class="spot">
                            <a href="<?php __p(esc_url(home_url('/project/the-sullivan/'))); ?>"><img src="<?php __a('/resources/artwork/logo-2.svg') ?>" /></a>
                            <div class="info">1, 2 and 3 bedrooom townhouses.</div>
                        </div>
                    </div>
                    <div class="bottom-box box">
                        <h5 class="header">Coming Soon</h5>
                        <div class="spot">
                            <a href="<?php __p(esc_url(home_url('/project/stage-3/'))); ?>">Stage<br />three</a>
                            <div class="info">1, 2 and 3 bedrooom apartments.</div>
                        </div>
                    </div>

                </div>
                <div class="r-box">
                    <div class="button font-nv-bold">
                        <a class="close btn-bg" href="#"><span>CLOSE</span></a>
                    </div>
                    <?php
                    wp_nav_menu(array(
                        'container' => false,
                        'container_class' => 'menu clearfix',
                        'theme_location' => 'main-nav'
                    ));
                    ?>

                </div>
            </div>

        </div>
    </aside>
    <div class="page-loading">
        <header id="masterhead" class="site-header" role="banner">
            <div class="outter">
                <nav id="site-navigation" class="navigation main-navigation desktop font-nv-bold" role="navigation">
                    <div class="logo">
                        <a href="<?php __p(esc_url(home_url('/'))); ?>" title="home">
                            <span></span>
                        </a>
                    </div>
                    <div class="button">
                        <a class="register" href="#register">Register</a>
                        <a class="menu btn-bg" href="#menu"><span>Menu</span></a>
                    </div>
                </nav>
            </div>
        </header>

        <div id="main" class="site-main">