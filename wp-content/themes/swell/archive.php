<?php get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php if (have_posts()) : ?>
				<header class="page-header">
					<h1 class="page-title">
						<?php
						if (is_day()) :
							printf('Daily Archives: %s', get_the_date());
						elseif (is_month()) :
							printf('Monthly Archives: %s', get_the_date('F Y'));
						elseif (is_year()) :
							printf('Yearly Archives: %s', get_the_date('Y'));
						else :
							__p('Archives');
						endif;
						?>
					</h1>
				</header>

				<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('content'); ?>
				<?php endwhile; ?>

				<?php sk_paging(); ?>
			<?php else : ?>
				<?php get_template_part('content', 'none'); ?>
			<?php endif; ?>

		</div>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>